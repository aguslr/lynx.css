---
---

## About ##

A *CSS* theme that changes the style of a website to match the look and feel of
the *text* web browser [Lynx][1].

*Lynx* is the oldest web browser (released in 1992) that is still commonly used
and in active development. For more information on what *Lynx* is and what it
looks like, visit [its wikipedia article][2].


## Features ##

*Lynx*'s behavior added via *CSS*:

- Monospace font for all elements.
- Matching color scheme.
- Matching text and element alignment.
- Block cursor next to focused link.


*Lynx*'s behavior added via *JavaScript*:

- Top bar with back button.
- Top bar with pager showing title, current page and number of pages of the
  document.
- Bottom bar with help message.
- Bottom bar with *URL* of focused link.
- Convert `<img>` *HTML* tags to links pointing to image file.
- Convert `<audio>` and `<video>` *HTML* tags to links pointing to media file.
- Convert `<iframe>` *HTML* tags to links pointing to *IFRAME* content.

*Lynx*'s behavior that was improved:

- Preserve alignment of table cells.


## Installation ##

Get the source code: <https://gitlab.com/aguslr/lynx.css>

To change the style of your website, **download** both the *CSS*
([`lynx.css`][3]) and the *JavaScript* ([`lynx.js`][4]) files into your
website's root directory. Then just add the following *HTML* code **between**
the `<head>` and `</head>` tags in each of your web pages:

```html
<link rel="stylesheet" href="lynx.css">
<script src="lynx.js"></script>
```


### Fonts ###

It's also recommended to add a good monospaced *CSS* font along with the
previous code so that everything lines up:

- [Fira Mono][5]:

    ```html
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Fira+Mono:500&display=swap">
    ```


## Screenshot ##

You can see a comparison between *Lynx* running in a terminal (left) and
*Firefox* (right), both displaying the [demo page][6]:

[![lynx-vs-firefox](//lynx.aguslr.com/assets/img/screenshot.png)][7]

Visit the [demo page][6] to see what a full-fledged *HTML* document (including
tables, lists, images, etc.) looks like when using the theme with both *CSS* and
*JavaScript* tweaks.


[1]: //lynx.invisible-island.net/
[2]: //en.wikipedia.org/wiki/Lynx_(web_browser)
[3]: //gitlab.com/aguslr/lynx.css/raw/main/lynx.css
[4]: //gitlab.com/aguslr/lynx.css/raw/main/lynx.js
[5]: //github.com/mozilla/Fira
[6]: //lynx.aguslr.com/demo.html
[7]: //lynx.aguslr.com/assets/img/screenshot.png
